// fs.cpp: File System

#include "sfs/fs.h"

#include <algorithm>

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include <iostream>
#include <string>
#include <cmath>

// Debug file system -----------------------------------------------------------

void FileSystem::debug(Disk *disk) {
    Block block;

    // Read Superblock
    disk->read(0, block.Data);

    printf("SuperBlock:\n");
    if(block.Super.MagicNumber == MAGIC_NUMBER){
        printf("    magic number is valid\n");
        printf("    %u blocks\n"         , block.Super.Blocks);
        printf("    %u inode blocks\n"   , block.Super.InodeBlocks);
        printf("    %u inodes\n"         , block.Super.Inodes);
    }
    
    // Read Inode blocks
    for (uint32_t i = 1; i <= block.Super.InodeBlocks; i++) {
        Block iblock;
        disk->read(i , iblock.Data);
        for (uint32_t j = 0; j < INODES_PER_BLOCK; j++) {
            if (iblock.Inodes[j].Valid){
                printf("Inode %d:\n", j);
                printf("    size: %d bytes\n", iblock.Inodes[j].Size);
                printf("    direct blocks:");
                for (uint32_t k = 0; k < POINTERS_PER_INODE; k++) {
                    if(iblock.Inodes[j].Direct[k]!=0){
                        printf(" %d", iblock.Inodes[j].Direct[k]);
                    }
                }
                printf("\n");
                
                if(iblock.Inodes[j].Indirect != 0){
                    printf("    indirect block: %d\n", iblock.Inodes[j].Indirect);
                    Block indiblock;
                    disk->read(iblock.Inodes[j].Indirect, indiblock.Data);
                    printf("    indirect data blocks:");
                    for (uint32_t k = 0; k < POINTERS_PER_BLOCK; k++) {
                        if(indiblock.Pointers[k] > 0 && indiblock.Pointers[k] < block.Super.Blocks){
                            printf(" %d", indiblock.Pointers[k]);
                        }
                    }
                    printf("\n");
                }
            }
        }
    }
}

// Format file system ----------------------------------------------------------

bool FileSystem::format(Disk *disk) {
    
    if (disk->mounted()) {
        return false;
    }
    
    // Write superblock
    Block block;
    
    memset(block.Data,0,disk->BLOCK_SIZE);
    block.Super.MagicNumber = MAGIC_NUMBER;
    block.Super.Blocks = disk->size();
    block.Super.InodeBlocks = (size_t)(((float)disk->size()*0.10)+0.5);
    block.Super.Inodes = INODES_PER_BLOCK*block.Super.InodeBlocks;
    disk->write(0, block.Data);

    // Clear all other blocks
    char c[BUFSIZ] = {0};
    for (size_t i=1; i<block.Super.Blocks; i++) {
        disk->write(i, c);
    }

    return true;
}

// Mount file system -----------------------------------------------------------

bool FileSystem::mount(Disk *disk) {
    
    if (disk->mounted()) {
        return false;
    }

    // Read superblock
    Block block;
    disk->read(0, block.Data);

    if ((block.Super.Inodes != block.Super.InodeBlocks * INODES_PER_BLOCK) || (block.Super.Blocks < 0) || (block.Super.MagicNumber != MAGIC_NUMBER) || (block.Super.InodeBlocks != ceil(0.10 * block.Super.Blocks))) {
        return false;
    }
    
    // Set device and mount
    disk->mount();
    this->fs_disk = disk;
        
    // Copy metadata
    fs_SuperBlock.Blocks = block.Super.Blocks;
    fs_SuperBlock.InodeBlocks = block.Super.InodeBlocks;
    fs_SuperBlock.Inodes = block.Super.Inodes;
    
    // Allocate free block bitmap
    freeBitmap = std::vector<int> (fs_SuperBlock.Blocks,1);

    for (uint32_t i = 0; i < fs_SuperBlock.Blocks; i++) {
        freeBitmap[i] = 1;
    }


    freeBitmap[0] = 0;

    for (uint32_t i = 0; i < fs_SuperBlock.InodeBlocks; i++) {
        freeBitmap[1+i] = 0;
    }


    for (uint32_t i = 0; i < fs_SuperBlock.InodeBlocks; i++) {
        Block iblock;
        disk->read(1+i,iblock.Data);

        for (uint32_t inode = 0; inode < INODES_PER_BLOCK; inode++) {

            if (!iblock.Inodes[inode].Valid) {
                continue;
            }
            uint32_t n = (uint32_t)ceil(iblock.Inodes[inode].Size/(double)disk->BLOCK_SIZE);
            for (uint32_t j = 0; j < POINTERS_PER_INODE && j < n; j++) {
                freeBitmap[iblock.Inodes[inode].Direct[j]] = 0;
            }

            if (n > POINTERS_PER_INODE) {
                Block indirect;
                disk->read(iblock.Inodes[inode].Indirect,indirect.Data);
                freeBitmap[iblock.Inodes[inode].Indirect] = 0;
                for (uint32_t k = 0; k < n - POINTERS_PER_INODE; k++) {
                    freeBitmap[indirect.Pointers[k]] = 0;
                }
            }
        }
    }

    return true;
}

// Create inode ----------------------------------------------------------------

ssize_t FileSystem::create() {
    // Locate free inode in inode table
    int inumber = -1;
    for (uint32_t i = 0; i < fs_SuperBlock.InodeBlocks; i++) {
        Block block;
        fs_disk->read(i+1,block.Data);
        for (uint32_t j = 0; j < INODES_PER_BLOCK; j++) {
            if (!block.Inodes[j].Valid) {
                inumber = j + INODES_PER_BLOCK*i;
                break;
            }
        }
        if (inumber != -1) {
            break;
        }
    }

    if (inumber == -1) {
        return -1;
    }
    
    // Record inode if found
    Inode inode;
    
    inode.Valid = true;
    inode.Size = 0;
    inode.Indirect = 0;
    
    for (uint32_t i = 0; i < POINTERS_PER_INODE; i++) {
        inode.Direct[i] = 0;
    }
    
    save_inode(inumber, &inode);

    return inumber;
}

// Remove inode ----------------------------------------------------------------

bool FileSystem::remove(size_t inumber) {
    // Load inode information
    Inode inode;

    // Load inode information
    if (!load_inode(inumber, &inode)) {
        return false;
    }
    if (inode.Valid == 0) {
        return false;
    }

    // Free direct blocks
    for (unsigned int i = 0; i < POINTERS_PER_INODE; i++) {
        if (inode.Direct[i] != 0) {
            freeBitmap[inode.Direct[i]] = 1;
            inode.Direct[i] = 0;
        }
    }

    // Free indirect blocks
    if (inode.Indirect != 0) {
        freeBitmap[inode.Indirect] = 1;
        Block block;
        fs_disk->read(inode.Indirect,block.Data);
        // Free blocks pointed to indirectly
        for (uint32_t i = 0; i < POINTERS_PER_BLOCK; i++) {
            if (block.Pointers[i] != 0) {
                freeBitmap[block.Pointers[i]] = 1;
            }
        }
    }
    // Clear inode in inode table
    inode.Indirect = 0;
    inode.Valid = 0;
    inode.Size = 0;
    if (!save_inode(inumber, &inode)) {
        return false;
    };

    return true;

}

// Inode stat ------------------------------------------------------------------

ssize_t FileSystem::stat(size_t inumber) {
    // Load inode information
    Inode inode;
    if (!load_inode(inumber,&inode)) {
        return -1;
    }
    
    if (!inode.Valid) {
        return -1;
    }

    return inode.Size;
}

// Read from inode -------------------------------------------------------------

ssize_t FileSystem::read(size_t inumber, char *data, size_t length, size_t offset) {

    // Load inode information
    Inode inode;
    
    if (!load_inode(inumber, &inode)) {
        return -1;
    }

    // Adjust length
    length = std::min(length, inode.Size - offset);

    // Read block and copy to data

    Block indiblock;
    
    if (((length + offset) / fs_disk->BLOCK_SIZE )> POINTERS_PER_INODE) {
        if (inode.Indirect == 0) {
            return -1;
        }
        fs_disk->read(inode.Indirect,indiblock.Data);
    }

    size_t read = 0;
    for (uint32_t i = (offset / fs_disk->BLOCK_SIZE); read < length; i++) {
        
        // figure out which block we're reading
        size_t readblock;
        if (i < POINTERS_PER_INODE) {
            readblock = inode.Direct[i];
        } else {
            readblock = indiblock.Pointers[i-POINTERS_PER_INODE];
        }

        if (readblock == 0) {
            return -1;
        }

        Block block;
        fs_disk->read(readblock,block.Data);
        size_t readoffset;
        size_t readlength;

        if (read == 0) {
            readoffset = offset % fs_disk->BLOCK_SIZE;
            readlength = std::min(fs_disk->BLOCK_SIZE - readoffset, length);
        }
        else {
            readoffset = 0;
            readlength = std::min(fs_disk->BLOCK_SIZE-readoffset, length-read);
        }
        memcpy(data + read, block.Data + readoffset, readlength);
        read += readlength;
    }
    return read;
}

// Write to inode --------------------------------------------------------------

ssize_t FileSystem::write(size_t inumber, char *data, size_t length, size_t offset) {

    // Load inode information
    Inode inode;
    if (!load_inode(inumber, &inode)){
        return -1;
    }
    size_t n = fs_disk->BLOCK_SIZE * (POINTERS_PER_INODE*POINTERS_PER_BLOCK);

    length = std::min(length, n-offset);
    
    Block indiblock;
    bool read = false;
    bool boolinode = false;
    bool boolindirect = false;

    // Write block and copy data
    size_t write = 0;
    for (uint32_t i = (offset / fs_disk->BLOCK_SIZE); write < length && i < POINTERS_PER_INODE * 2; i++) {
        size_t writeblock;
        if (i < POINTERS_PER_INODE) {
            if (inode.Direct[i] == 0) {
                ssize_t a = allocate_free_block();
                if (a == -1) {
                    break;
                }
                inode.Direct[i] = a;
                boolinode = true;
            }
            writeblock = inode.Direct[i];
        } else {
            if (inode.Indirect == 0) {
                ssize_t a = allocate_free_block();
                if (a == -1) {
                    return write;
                }
                inode.Indirect = a;
                boolindirect = true;
            }

            if (!read) {
                fs_disk->read(inode.Indirect,indiblock.Data);
                read = true;
            }

            if (indiblock.Pointers[i - POINTERS_PER_INODE] == 0) {
                ssize_t a = allocate_free_block();
                if (a == -1) {
                    break;
                }
                indiblock.Pointers[i - POINTERS_PER_INODE] = a;
                boolindirect = true;
            }
            writeblock = indiblock.Pointers[i-POINTERS_PER_INODE];
        }

        size_t writeoffset;
        size_t writelength;

        if (write == 0) {
            writeoffset = offset % fs_disk->BLOCK_SIZE;
            writelength = std::min(fs_disk->BLOCK_SIZE - writeoffset, length);
        } else {
            writeoffset = 0;
            writelength = std::min(fs_disk->BLOCK_SIZE-writeoffset, length-write);
        }

        char writebuffer[fs_disk->BLOCK_SIZE];

        if (writelength < fs_disk->BLOCK_SIZE) {
            fs_disk->read(writeblock,(char*)writebuffer);
        }

        memcpy(writebuffer + writeoffset, write + data, writelength);
        fs_disk->write(writeblock,(char*)writebuffer);
        write += writelength;
    }

    uint32_t m = std::max((size_t)inode.Size, write + offset);
    if (m != inode.Size) {
        inode.Size = m;
        boolinode = true;
    }
    
    if (boolinode) {
        save_inode(inumber,&inode);
    }
     
    if (boolindirect) {
        fs_disk->write(inode.Indirect,indiblock.Data);
    }

    return write;
}

size_t FileSystem::allocate_free_block(){
    int b = -1;
    for (uint32_t i = 0; i < fs_SuperBlock.Blocks; i++) {
        if (freeBitmap[i]) {
            freeBitmap[i] = 0;
            b = i;
            break;
        }
    }

    // need to zero data block if we're allocating one
    if (b != -1) {
        char data[fs_disk->BLOCK_SIZE];
        memset(data,0,fs_disk->BLOCK_SIZE);
        fs_disk->write(b,(char*)data);
    }

    return b;
}

bool FileSystem::load_inode(size_t inumber, Inode *node) {
    size_t nblock = inumber / INODES_PER_BLOCK + 1;
    size_t inode_offset = inumber % INODES_PER_BLOCK;

    if (inumber >= fs_SuperBlock.Inodes) {
        return false;
    }

    Block block;
    
    fs_disk->read(nblock,block.Data);
    *node = block.Inodes[inode_offset];

    return true;
}

bool FileSystem::save_inode(size_t inumber, Inode *node){
 
    size_t nblock = inumber / INODES_PER_BLOCK + 1;
    size_t offset = inumber % INODES_PER_BLOCK;

    if (inumber >= fs_SuperBlock.Inodes) {
        return false;
    }

    Block block;
    fs_disk->read(nblock,block.Data);
    block.Inodes[offset] = *node;
    fs_disk->write(nblock,block.Data);

    return true;
}
